package orders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.bean.CsvToBean;
import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy;

public class DataAccess {

	public int getPromotionalDiscount(String couponCode) throws Exception {
		simulateDelay();
		CSVReader reader = null;
		try {
			reader = createCSVReader("discounts.txt");
			HeaderColumnNameMappingStrategy<Discount> strategy = new HeaderColumnNameMappingStrategy<Discount>();
			strategy.setType(Discount.class);
			CsvToBean<Discount> csv = new CsvToBean<Discount>();
			List<Discount> discounts = csv.parse(strategy, reader);
			for (Discount discount : discounts) {
				if (discount.getCoupon().equals(couponCode)) {
					return discount.getPercentage();
				}
			}
			throw new Exception("Coupon not found");
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	public Order getOrder(int id) throws Exception {
		simulateDelay();
		CSVReader reader = null;
		try {
			reader = createCSVReader("orders.txt");
			HeaderColumnNameMappingStrategy<Order> strategy = new HeaderColumnNameMappingStrategy<Order>();
			strategy.setType(Order.class);
			CsvToBean<Order> csv = new CsvToBean<Order>();
			List<Order> orders = csv.parse(strategy, reader);
			for (Order order : orders) {
				if (order.getId() == id) {
					return order;
				}
			}
			return null;
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	public void saveOrder(Order order) throws Exception {
		simulateDelay();
		List<Order> orders = new ArrayList<Order>();
		CSVReader reader = null;
		try {
			reader = createCSVReader("orders.txt");
			HeaderColumnNameMappingStrategy<Order> strategy = new HeaderColumnNameMappingStrategy<Order>();
			strategy.setType(Order.class);
			CsvToBean<Order> csv = new CsvToBean<Order>();
			orders = csv.parse(strategy, reader);
		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		Order orderSaved = null;
		for (Order or : orders) {
			if (or.getId() == order.getId())
				orderSaved = order;
		}
		if (orderSaved != null)
			throw new Exception(
					"Primary Constraint Exception: another object already exists with same Id");
		orders.add(order);

		CSVWriter writer = null;
		try {
			writer = createCSVWriter("orders.txt");
			writer.writeNext(new String[] { "Id", "ItemTotal", "Total",
					"Coupon" });
			for (Order or : orders) {
				writer.writeNext(new String[] { String.valueOf(or.getId()),
						String.valueOf(or.getItemTotal()),
						String.valueOf(or.getTotal()), or.getCoupon() });
			}
			writer.flush();
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	private CSVWriter createCSVWriter(String fileName) throws Exception {
		File file = getFile(fileName);
		OutputStream output = new FileOutputStream(file);
		return new CSVWriter(new OutputStreamWriter(output), ',', '\0');
	}

	private CSVReader createCSVReader(String fileName) throws Exception {
		File file = getFile(fileName);
		InputStream input = new FileInputStream(file);
		return new CSVReader(new InputStreamReader(input), ',', '\0');
	}

	private File getFile(String fileName) throws Exception {
		URL resourceUrl = getClass().getProtectionDomain().getCodeSource().getLocation();
		File file = new File(resourceUrl.toURI());
		String dirPath = file.getParentFile().getParent();
		return new File(dirPath + "/data/" + fileName);
	}

	private void simulateDelay() throws Exception {
		int min = 4000;
		int max = 8000;
		Random rand = new Random();
		int miliseconds = rand.nextInt((max - min) + 1) + min;
		Thread.sleep(miliseconds);
	}
}
