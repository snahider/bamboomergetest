package orders;


public class OrderServices {
	private DataAccess dataAccess;

	public OrderServices() {
		dataAccess = new DataAccess();
	}

	public double calculateTotal(Order order) throws Exception {
		double itemTotal = order.getItemTotal();

		int discountPercentage = 0;
		if (order.getCoupon() != null) {
			discountPercentage = getPromotionalDiscount(order.getCoupon());
		}

		double discount = itemTotal * discountPercentage / 100;
		return itemTotal - discount;
	}

	protected int getPromotionalDiscount(String coupon) throws Exception {
		return dataAccess.getPromotionalDiscount(coupon);
	}

	public Order getOrder(int id) throws Exception {
		return dataAccess.getOrder(id);
	}

	public void save(Order order) throws Exception {
		if (!isValid(order)) {
			throw new Exception("Invalid Order");
		}
		persistOrder(order);
	}

	protected void persistOrder(Order order) throws Exception {
		dataAccess.saveOrder(order);
	}

	private boolean isValid(Order order) {
		return order.getId() > 0 && order.getItemTotal() != 0
				&& order.getTotal() != 0;
	}
}
