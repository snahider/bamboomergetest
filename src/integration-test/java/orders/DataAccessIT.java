package orders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

//Estas no son realmente pruebas de integracion =)
public class DataAccessIT{
	
	@Test
	public void calculateTotal_withoutCoupon_returnLineItemTotal()
			throws Exception {
		Order order = new Order(1, null, 100);
		OrderServices orderServices = new OrderServices();

		double total = orderServices.calculateTotal(order);

		assertEquals(100,total,0);
	}

	@Test
	public void calculateTotal_withCoupon_returnLineItemWithDiscount()
			throws Exception {
		Order order = new Order(1, "christmas", 100);
		TestableOrderServicesIT orderServices = new TestableOrderServicesIT();
		orderServices.discount=20;

		double total = orderServices.calculateTotal(order);

		assertEquals(80,total,0);
	}
	
	@Test
	public void save_validOrder_theOrderIsPersisted() throws Exception {
		Order order = new Order(1, null, 100);
		order.setTotal(100);
		TestableOrderServicesIT orderServices = new TestableOrderServicesIT();

		orderServices.save(order);

		assertTrue(orderServices.orderPersisted);
	}
	
	private class TestableOrderServicesIT extends OrderServices{
		public int discount;
		public boolean orderPersisted=false;
		
		@Override
		protected int getPromotionalDiscount(String coupon) throws Exception {
			return discount;
		}
		
		@Override
		protected void persistOrder(Order order) throws Exception {
			orderPersisted=true;
		}
	}
}
